// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCKw2Ni1XCdT50FB9skcAy4xJ24sXtGXwA",
    authDomain: "uit-companion.firebaseapp.com",
    databaseURL: "https://uit-companion.firebaseio.com",
    projectId: "uit-companion",
    storageBucket: "uit-companion.appspot.com",
    messagingSenderId: "386076590654"
  }
};
