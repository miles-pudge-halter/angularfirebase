import { Injectable } from '@angular/core';
// import { AngularFireModule } from 'angularfire2';
// import { AngularFireDatabaseModule } from 'angularfire2/database';
// import { AngularFireAuthModule } from 'angularfire2/auth';
// import { environment } from '../../environments/environment';
import { AngularFireDatabase , FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import * as firebase from 'firebase';

@Injectable()
export class FirebaseServiceService {

  listings: FirebaseListObservable<any[]>;
  listing: FirebaseObjectObservable<any>;

  allClasses: FirebaseObjectObservable<any[]>;

  folder: any;
  constructor(private af: AngularFireDatabase) {
    this.folder='listingImages';
    this.listings=this.af.list('/listings') as FirebaseListObservable<List[]>;
    
   }

  getListings(){
    return this.listings;
  }
  getListingDetails(id){
    this.listing=this.af.object('/listings/'+id) as FirebaseObjectObservable<List>;
    return this.listing;
  }

  addListing(listing){
    //Create root ref
    let storageRef = firebase.storage().ref();
    for(let selectedFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]){
      let path=`${this.folder}/${selectedFile.name}`;
      let iRef=storageRef.child(path);
      iRef.put(selectedFile).then((snapshot)=>{
        listing.image=selectedFile.name;
        listing.path=path;
        return this.listings.push(listing);
      })
    }
  }

  updateListing(id, listing){
    return this.listings.update(id,listing);
  }

  deleteListing(id){
    return this.listings.remove(id);
  }
// --------- FOR TIMETABLE ------------
  getTimetable(path){
    console.log(path);
    this.allClasses=this.af.object('/Timetable/'+path) as FirebaseObjectObservable<uniClasses[]>;
    return this.allClasses;
  }
}

interface List{
  $key?:string;
  title?:string;
  type?:string;
  image?:string;
  city?:string;
  owner?:string;
  bedrooms?:string;
  path?:string;
}
interface uniClasses{
  $key:any;
  rowClasses: rowClasses;
}
interface rowClasses{
  $key:any;
  Lecturer: string;
  Name: string;
  Room: string;
  subjID: string;
}