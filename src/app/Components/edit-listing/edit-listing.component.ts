import { Component, OnInit } from '@angular/core';
import { FirebaseServiceService } from '../../Services/firebase-service.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-listing',
  templateUrl: './edit-listing.component.html',
  styleUrls: ['./edit-listing.component.css']
})
export class EditListingComponent implements OnInit {
  id:any;
  title:any;
  owner:any;
  city:any;
  bedrooms:any;
  price:any;
  image:any;
  type:any;

  constructor(
    private firebaseService : FirebaseServiceService,
    private router : Router,
    private route : ActivatedRoute
  ) { }

  ngOnInit() {
    this.id=this.route.snapshot.params['id'];

    this.firebaseService.getListingDetails(this.id)
      .subscribe(listing=>{
        this.title=listing.title;
        this.owner=listing.owner;
        this.city=listing.city;
        this.bedrooms=listing.bedrooms;
        this.price=listing.price;
        this.type=listing.type;
      });
  }

  onEditSubmit(){
    let listing = {
      title : this.title,
      owner : this.owner,
      city : this.city,
      bedrooms : this.bedrooms,
      price : this.price,
      type : this.type
    }
    this.firebaseService.updateListing(this.id, listing);
    this.router.navigate(['/list/',this.id]);
  }

}
