import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(public af:AngularFireAuth, public flashMessage: FlashMessagesService) { }

  ngOnInit() {
  }
  login(){
    this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  logout(){
    this.af.auth.signOut();
    this.flashMessage.show('You are logged out',
      {cssClass: 'alert-danger', timeout:2000});
  }
}
