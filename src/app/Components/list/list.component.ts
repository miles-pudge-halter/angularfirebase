import { Component, OnInit } from '@angular/core';
import { FirebaseServiceService } from '../../Services/firebase-service.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  list:any;
  isLoading:boolean;
  constructor(private firebaseService: FirebaseServiceService) { }

  ngOnInit() {
    this.isLoading=true;
    this.firebaseService.getListings()
      .subscribe(listings=>{
        console.log(listings);
        this.isLoading=false;
        this.list=listings;
      })
  }

}
