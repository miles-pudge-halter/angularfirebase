import { Component, OnInit } from '@angular/core';
import { FirebaseServiceService } from '../../Services/firebase-service.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { InitDropdownDirective } from '../init-dropdown.directive';
import * as firebase from 'firebase/app';
declare var $: any;

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css']
})
export class TimetableComponent implements OnInit {
  year:string;
  major:string;
  section:string;
  path:string;
  isLoading:boolean;

  uniClasses:any;

  mondayClasses:any[];
  tuesdayClasses:any[];
  wednesdayClasses:any[];
  thursdayClasses:any[];
  fridayClasses:any[];

  freeClass:colClasses;

  constructor(
    private firebaseService : FirebaseServiceService,
    private router : Router,
    private route : ActivatedRoute,
    private angularAuth : AngularFireAuth
  ) { 
        this.route.queryParams.subscribe(params=>{
          if(params['year']){
            //---- To fill auto-fill the form from params ------
            this.year=params['year'];
            this.major=params['major'];
            this.section=params['section'];
            //--------------------------------------------------

            //----- Concat path --------------------------------
            this.path=params['year'];
            if(params['major']!='none') 
              this.path=this.path+'/'+params['major'];
            if(params['section']!='none')
              this.path=this.path+'/'+params['section'];
            //--------------------------------------------------
          }
        });
  }

  ngOnInit() {
    this.year="Fifth";
    this.section="none";
    this.major="CS/SE";
    if(this.angularAuth.auth.currentUser){
      this.search();
      console.log(this.uniClasses);
    }
    
  }
  login(){
    this.angularAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  search():void{
    this.mondayClasses=[];
        this.tuesdayClasses=[];
        this.wednesdayClasses=[];
        this.thursdayClasses=[];
        this.fridayClasses=[];
    console.log("PATH: ",this.path);
    if(!this.path){
      return;
    }

    //$('.ui.modal').modal('show');
    //---- Show spinner ----
    this.isLoading=true;
    //----------------------

    this.firebaseService.getTimetable(this.path)
      .subscribe(uniClasses=>{
        //---- Hide spinner ----
        this.isLoading=false;
        //----------------------
        this.uniClasses=uniClasses;
        console.log("SINGLE",this.uniClasses);

        //------- Empty Class -------
        this.freeClass={
          Lecturer:'',Name:'Free',subjID:'',Room:''
        };
        //---------------------------

        //----------MONDAY----------
        for(let i=1;i<=7;i++){
          if(this.uniClasses.Monday[`${i}`]){
            this.mondayClasses.push(this.uniClasses.Monday[`${i}`]);
          }
          else{
            this.mondayClasses.push(this.freeClass);
          }
        }
        //--------------------------
        //----------Tuesday----------
        for(let i=1;i<=7;i++){
          if(this.uniClasses.Tuesday[`${i}`]){
            this.tuesdayClasses.push(this.uniClasses.Tuesday[`${i}`]);
          }
          else{
            this.tuesdayClasses.push(this.freeClass);
          }
        }
        //---------------------------
        //----------WEDNESDAY----------
        for(let i=1;i<=7;i++){
          if(this.uniClasses.Wednesday[`${i}`]){
            this.wednesdayClasses.push(this.uniClasses.Wednesday[`${i}`]);
          }
          else{
            this.wednesdayClasses.push(this.freeClass);
          }
        }
        //-----------------------------
        //----------Thursday----------
        for(let i=1;i<=7;i++){
          if(this.uniClasses.Thursday[`${i}`]){
            this.thursdayClasses.push(this.uniClasses.Thursday[`${i}`]);
          }
          else{
            this.thursdayClasses.push(this.freeClass);
          }
        }
        //----------------------------
        //----------Friday---------
        for(let i=1;i<=7;i++){
          if(this.uniClasses.Friday[`${i}`]){
            this.fridayClasses.push(this.uniClasses.Friday[`${i}`]);
          }
          else{
            this.fridayClasses.push(this.freeClass);
          }
        }
        //--------------------------

      });
  }
  onTimetableSubmit(){
    //------------ recall route with params ------------
    this.router.navigate(
      ['/timetable'],
      {queryParams:
        {year: this.year, major:this.major, section:this.section}
      }
    ).then(_=>this.search());
    //--------------------------------------------------
  }

}
interface rowClasses{
  day:string;
  colClass:colClasses[];
}
interface colClasses{
  Lecturer: string;
  Name: string;
  Room: string;
  subjID: string;
}
