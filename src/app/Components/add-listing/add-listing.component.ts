import { Component, OnInit } from '@angular/core';
import { FirebaseServiceService } from '../../Services/firebase-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-listing',
  templateUrl: './add-listing.component.html',
  styleUrls: ['./add-listing.component.css']
})
export class AddListingComponent implements OnInit {
  title:any;
  city:any;
  owner:any;
  bedrooms:any;
  price:any;
  type:any;
  image:any;

  constructor(
    private firebaseService : FirebaseServiceService,
    private router : Router
  ) { }

  ngOnInit() {
  }
  onAddSubmit(){
    let listing={
      title: this.title,
      city: this.city,
      owner: this.owner,
      bedrooms: this.bedrooms,
      price: this.price,
      type: this.type
    }
    this.firebaseService.addListing(listing);
    this.router.navigate(['/list']);
  }
}
