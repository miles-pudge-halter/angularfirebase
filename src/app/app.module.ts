import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './Components/home/home.component';
import { NavBarComponent } from './Components/nav-bar/nav-bar.component';
import { ListComponent } from './Components/list/list.component';
import { ListingComponent } from './Components/listing/listing.component';
import { AddListingComponent } from './Components/add-listing/add-listing.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { RouterModule, Routes } from '@angular/router';
import { FlashMessagesModule } from 'angular2-flash-messages';

import { FirebaseServiceService } from './Services/firebase-service.service';
import { EditListingComponent } from './Components/edit-listing/edit-listing.component';
import { TimetableComponent } from './Components/timetable/timetable.component';
import { TimetableRowComponent } from './Components/timetable-row/timetable-row.component';
import { ObjNgForPipe } from './Components/obj-ng-for.pipe';
import { InitDropdownDirective } from './Components/init-dropdown.directive';

const appRoutes:Routes = [
  {path: '', component: HomeComponent},
  {path:'list', component: ListComponent},
  {path:'list/:id', component: ListingComponent},
  {path:'add-listing', component: AddListingComponent},
  {path:'edit-listing/:id', component: EditListingComponent},
  {path:'timetable', component:TimetableComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavBarComponent,
    ListComponent,
    ListingComponent,
    AddListingComponent,
    EditListingComponent,
    TimetableComponent,
    TimetableRowComponent,
    ObjNgForPipe,
    InitDropdownDirective
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FlashMessagesModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [FirebaseServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
